import asyncpg

async def init_db(conn: asyncpg.Connection):
    await User.create_table(conn)

class User(object):
    id: int
    first_name: str
    last_name: str
    login: str

    @classmethod
    async def create_table(cls, conn: asyncpg.Connection):
        return await conn.execute('''
                create table users (
                    id int primary key,
                    first_name varchar(100) not null,
                    last_name varchar(100) not null,
                    login varchar(32) not null
                );
        ''')


class Organization(object):
    id: int
    admin_id: int  # user_id
    name: str
    slug: str
    balance: float


class UserToOrganization(object):
    user_id: int
    organization_id: int
    position: str


class Project(object):
    id: int
    organization_id: int
    slug: str
    name: str
    parent_project_id: str


class UserIsPMOfProject(object):
    user_id: int
    project_id: int


class Transaction(object):
    id: int
    project_id: int
    user_id: int
    amount: float
    direction: int  # -1 or 1
    contractor: str
    description: str
    payment_type: str  # с НДС без счёта, с НДС по счёту, без счёта
    payment_details: str  # Номер карты / счёта и т.п.
    status: int  # PLANNED, PENDING, COMPLETED
